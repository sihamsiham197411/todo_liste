<?php
// Database connection parameters
define('DB_USER', 'root');
define('DB_PASS', '');
define('DB_NAME', 'todolist');
define('DB_HOST', '127.0.0.1');
define('DB_PORT', '3308');

// Create a database connection
$conn = new mysqli(DB_HOST, DB_USER, DB_PASS, DB_NAME, DB_PORT);

// Check the connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}



// a. Read tasks and sort by creation date


// b. Handle actions
if ($_SERVER["REQUEST_METHOD"] == "POST") {
    if (isset($_POST["action"])) {
        $action = $_POST["action"];
        
        switch ($action) {
            case "new":
                if (isset($_POST["title"])) {
                    $title = $_POST["title"];
                    $sql = "INSERT INTO todo (title) VALUES ('$title')";
                    $conn->query($sql);
                }
                break;
            case "delete":
                if (isset($_POST["id"])) {
                    $id = $_POST["id"];
                    $sql = "DELETE FROM todo WHERE id=$id";
                    $conn->query($sql);
                }
                break;
            case "toggle":
                if (isset($_POST["id"])) {
                    $id = $_POST["id"];
                    $sql = "UPDATE todo SET done = 1 - done WHERE id=$id";
                    $conn->query($sql);
                }
                break;
            // Add more cases for other actions if needed
        }
    }
}

// Close the database connection
?>

<!-- index.php -->

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Todo List</title>
    <!-- Bootstrap CSS -->
    <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" rel="stylesheet">
</head>
<body>
    <!-- Navbar -->
    <nav class="navbar navbar-expand-lg navbar-light bg-light">
        <a class="navbar-brand" href="#">Todo List</a>
    </nav>

    <div class="container mt-4">
        <!-- New Task Form -->
        <form method="post" action="">
            <div class="form-group">
                <label for="title">New Task:</label>
                <input type="text" class="form-control" name="title" required>
            </div>
            <button type="submit" class="btn btn-primary" name="action" value="new">Add</button>
        </form>

        <!-- Task List -->
        <?php include "listetaches.php"?>
    </div>

    <!-- Bootstrap JS and jQuery (required for Bootstrap) -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
</body>
</html>
