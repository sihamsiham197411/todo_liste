<?php 
$sql = "SELECT * FROM todo ORDER BY created_at DESC";
$result = $conn->query($sql);
$taches = array();
if ($result->num_rows > 0) {
    while ($row = $result->fetch_assoc()) {
        $taches[] = $row;
    }
}

?>
<ul class="list-group mt-4">
            <?php foreach ($taches as $tache): ?>
                <li class="list-group-item <?php echo ($tache['done'] == 1) ? 'list-group-item-success' : 'list-group-item-warning'; ?>">
                    <?php echo $tache["title"]; ?>
                    <form method="post" action="">
                        <input type="hidden" name="id" value="<?php echo $tache["id"]; ?>">
                        <button type="submit" class="btn btn-secondary" name="action" value="toggle">Toggle</button>
                        <button type="submit" class="btn btn-danger" name="action" value="delete">Delete</button>
                    </form>
                </li>
            <?php endforeach; ?>
        </ul>